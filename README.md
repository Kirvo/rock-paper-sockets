# Rock, Paper, Sockets!

This is a small project with I have learned to use sockets and work with Node, Socket.io and other technologies. 

To start, within the project path, execute the following commands on different terminals in both client and server folders:

```
npm install
npm start
```

You should go to client/src/components/Game/Game.js and update ENDPOINT const to match your desired url specified on PORT const, on server/index.js (as localhost:3100)

When you have it open, you'll have to open two different browsers! This way you can test as if you were the two clients (For now it's nothing functional, I'm working on improvements for the project ;) ) Each player will have to click on a card and click on the "play" button. When both of them have pressed the button, the server will calculate the winner and update the score.

**Todo (not in priority order)**
- [x] Deploy on Netlify and Heroku
- [x] Use React.js on client side
- [ ] UI Improvements
- [ ] Add victory/defeat message
- [ ] Private rooms and search quick match
- [ ] Use GitBook to documentate both server and client sides


This is a project based on the [Socket.io Chat Demo](https://github.com/socketio/socket.io/tree/master/examples/chat) and [project of adrianhajdin](https://github.com/adrianhajdin/project_chat_application)