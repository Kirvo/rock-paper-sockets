import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';

import './Game.css';
import Notification from '../Notification/Notification.js';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

let socket;

const Game = ({ location }) => {
    const [room, setRoom] = useState('');

    const [playerNames, setPlayerNames] = useState(['', '']);
    const [score, setScore] = useState([0, 0]);
    const [icons, setIcons] = useState(['dragon', 'otter']);

    const [rockStyle, setRockStyle] = useState('card');
    const [paperStyle, setPaperStyle] = useState('card');
    const [scissorsStyle, setScissorsStyle] = useState('card');
    const [cardSelected, setCardSelected] = useState('');

    const [animationText, setAnimationText] = useState('Hola!');
    const [animationEffect, setAnimationEffect] = useState('connection');
    const [animationToggle, setAnimationToggle] = useState(false);


    const ENDPOINT = 'https://rockpapersockets.herokuapp.com/';


    useEffect(() => {
        const { username, room, icon } = queryString.parse(location.search);

        socket = io(ENDPOINT);

        setRoom(room);

        socket.emit('join', {username, room, icon}, (error) => {
            if(error) alert(error);
        });
    }, [ENDPOINT, location.search]);

    useEffect(() => {
        socket.on('get gameData', (data) => {
            console.log("He recibido datos!!", data)

            setPlayerNames(data.names);
            setScore(data.score);
            setIcons(data.icons);
        });

        socket.on('get connection', getConnection);
        socket.on('get disconnection', getDisconnection);
    }, []);

    useEffect(() => {
        if(!icons) { setIcons(['dragon', 'otter']); }
        else if(!icons[0] || icons[0] === '') setIcons(['dragon', icons[1]]);
        else if(!icons[1] || icons[1] === '') setIcons([icons[0], 'otter']);
    }, [icons])

    const selectCard = (type) => {        
        if(type === "Rock") {
            setRockStyle('card selected');
            if(cardSelected === "Paper") setPaperStyle('card');
            if(cardSelected === "Scissors") setScissorsStyle('card');
        
            setCardSelected("Rock");
        }
    
        if(type === "Paper") {
            setPaperStyle('card selected');
            if(cardSelected === "Rock") setRockStyle('card');
            if(cardSelected === "Scissors") setScissorsStyle('card');
        
            setCardSelected("Paper");
        }
    
        if(type === "Scissors") {
            setScissorsStyle('card selected');
            if(cardSelected === "Rock") setRockStyle('card');
            if(cardSelected === "Paper") setPaperStyle('card');
        
            setCardSelected("Scissors");
        }
    }
    
    const play = () => {
        if(cardSelected !== '') {
            socket.emit("play card", {room, card: cardSelected});
    
            if(cardSelected === "Rock") setRockStyle('card');
            if(cardSelected === "Paper") setPaperStyle('card');
            if(cardSelected === "Scissors") setScissorsStyle('card');
        
            setCardSelected("");
        }
    }

    const getConnection = (text) => {        
        setAnimationText(text);
        setAnimationEffect('connection');
        setAnimationToggle(true);
    
        setTimeout(() => {
            setAnimationToggle(false);
            setAnimationEffect('');
        }, 3000);       
    }

    const getDisconnection = (text) => {
        setAnimationText(text);
        setAnimationEffect('disconnection');
        setAnimationToggle(true);
    
        setTimeout(() => {
            setAnimationToggle(false);
            setAnimationEffect('');
        }, 3000);     
    }


    return (
        <div className="gameContainer">
            <div className="notification-wrapper">
                <Notification text={animationText} animation={animationEffect} animate={animationToggle} />
            </div>

            <div className="gameArea">
                <div className="score">
                    <div className="player1">
                        <div className="player1-icon">
                            <FontAwesomeIcon icon={icons[0]} />
                        </div>
                        <div className="player1-name">{playerNames[0]}</div>
                    </div>

                    <div className="points">
                        {score[0]} : {score[1]}
                    </div>

                    <div className="player2">
                        <div className="player2-icon">              
                            <FontAwesomeIcon icon={icons[1]} />
                        </div>
                        <div className="player2-name">{playerNames[1]}</div>
                    </div>
                </div>

                <div className="cards">
                    <button className={rockStyle} onClick={() => selectCard('Rock')}>
                        <FontAwesomeIcon icon="hand-rock" size="8x" />
                        <div className="card-text">Rock</div>
                    </button>

                    <button className={paperStyle} onClick={() => selectCard('Paper')}>
                        <FontAwesomeIcon icon="hand-paper" size="8x" />
                        <div className="card-text">Paper</div>
                    </button>
                    
                    <button className={scissorsStyle} onClick={() => selectCard('Scissors')}>
                        <FontAwesomeIcon icon="hand-scissors" size="8x" />
                        <div className="card-text">Scissors</div>
                    </button>
                </div>
                
                <button className="btn play" onClick={play}>Play</button>
            </div>
        </div>
    );
};


export default Game;