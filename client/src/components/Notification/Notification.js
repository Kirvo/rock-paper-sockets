import React, { useState, useEffect } from 'react';

import './Notification.css';



const Notification = (props) => {
    const [text, setText] = useState('');
    const [animation, setAnimation] = useState('connection');
    const [animate, toggleAnimate] = useState(false);

    useEffect(() => {    
        setText(props.text);
    }, [props.text]);

    useEffect(() => {
        setAnimation(props.animation);
    }, [props.animation]);

    useEffect(() => {
        toggleAnimate(props.animate);
    }, [props.animate]);

    return (        
        <div className={'notification ' + animation + (animate ? ' animate' : '')}>
            <div className="text">{text}</div>
        </div>
    );
};

export default Notification;