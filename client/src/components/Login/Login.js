import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import './Login.css';

const ICON = {
    otter: 'otter',
    hippo: 'hippo',
    dog: 'dog',
    kiwi: 'kiwi',
    frog: 'frog',
    dragon: 'dragon',
    crow: 'crow',
    dove: 'dove',
    cat: 'cat',
    fish: 'fish',
    horse: 'horse',
    spider: 'spider',
}

const Login = () => {
    const [username, setUsername] = useState('');
    const [room, setRoom] = useState('');
    const [icon, setIcon] = useState(ICON.dragon);


    return (
        <div className="loginContainer">
            <h1 className="title">Login</h1>
            
            <div className="loginInputContainer">
                <input placeholder="Username" type="text" maxLength="14" className="loginInputItem" onChange={event =>  setUsername(event.target.value) } />
                <input placeholder="Room" type="text" className="loginInputItem" onChange={event => setRoom(event.target.value) } />
                <select className="loginInputItem" value={icon} onChange={(event) => setIcon(event.target.value)}>
                    <option value={ICON.otter}>{ICON.otter}</option>
                    <option value={ICON.hippo}>{ICON.hippo}</option>
                    <option value={ICON.dog}>{ICON.dog}</option>
                    <option value={ICON.kiwi}>{ICON.kiwi}</option>
                    <option value={ICON.frog}>{ICON.frog}</option>
                    <option value={ICON.dragon}>{ICON.dragon}</option>
                    <option value={ICON.crow}>{ICON.crow}</option>
                    <option value={ICON.dove}>{ICON.dove}</option>
                    <option value={ICON.cat}>{ICON.cat}</option>
                    <option value={ICON.fish}>{ICON.fish}</option>
                    <option value={ICON.horse}>{ICON.horse}</option>
                    <option value={ICON.spider}>{ICON.spider}</option>
                </select>
            </div>
            
            <Link onClick={event => (!username || !room) ? event.preventDefault() : null} to={`/game?username=${username}&room=${room}&icon=${icon}`}>
                <button className="loginButton" type="submit">Log in</button>
            </Link>      
        </div>
    );
};

export default Login;