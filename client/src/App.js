import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { library } from '@fortawesome/fontawesome-svg-core';
import { 
    faHandRock, faHandPaper, faHandScissors,
    faDragon, faOtter, faHippo, faDog, faKiwiBird, faFrog,
    faCrow, faDove, faCat, faFish, faHorse, faSpider,
} from '@fortawesome/free-solid-svg-icons';

import Login from './components/Login/Login';
import Game from './components/Game/Game';

import './App.css';

library.add(
    faHandRock, faHandPaper, faHandScissors,
    faDragon, faOtter, faHippo, faDog, faKiwiBird, faFrog,
    faCrow, faDove, faCat, faFish, faHorse, faSpider,
)

const App = () => {
    return (
        <Router>
            <Route path="/" exact component={Login}></Route>
            <Route path="/game" component={Game}></Route>
        </Router>
    )
};

export default App;