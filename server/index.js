const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const PORT = process.env.PORT || 3100;

const router = require('./router');

const { addUser, removeUser, getUser } = require('./users.js');
const {
    addGame, updateScoreOfPlayer, updateCardOfPlayer, getIconsInGame,
    addPlayerToGame, removePlayerInGame, getPlayersInGame, getNamesInGame, getScoreInGame
} = require('./games.js');


app.use(cors());
app.use(router);


io.on('connection', (socket) => {

    /** Connection events */

    socket.on('join', ({ username, room, icon}, callback) => {
        const { error, user } = addUser({ id: socket.id, username, room, icon });       

        if(error) return callback(error);

        const { err, game } = addGame({ id: user.room });

        addPlayerToGame(user.room, {id: user.id, score: 0, card: '', username: user.username, icon});


        socket.join(user.room);

        io.to(user.room).emit('get gameData', { room: user.room, names: getNamesInGame(user.room), score: getScoreInGame(user.room), icons: getIconsInGame(user.room) });
        io.to(user.room).emit('get connection', `${user.username} se ha conectado.`);
    })

    socket.on("disconnect", () => {
        const userToDisconnect = getUser(socket.id);

        if(userToDisconnect) {
            removeUser(userToDisconnect.id);
            removePlayerInGame(userToDisconnect.room, userToDisconnect.id);

            // Reseteamos las puntuaciones para el próximo jugador de la partida
            io.to(userToDisconnect.room).emit('get gameData', { room: userToDisconnect.room, names: getNamesInGame(userToDisconnect.room), score: [0, 0], icons: getIconsInGame(userToDisconnect.room) });
            io.to(userToDisconnect.room).emit('get disconnection', `${userToDisconnect.username} se ha desconectado.`);
        }
    });


    /** Play events */   

    socket.on("play card", ({room, card}) => {
        updateCardOfPlayer(room, socket.id, card);
        
        // Si los dos han jugado su carta, calculamos resultados
        if(getResults(room)) {
            io.to(room).emit('get gameData', 
                { room, names: getNamesInGame(room), score: getScoreInGame(room), icons: getIconsInGame(room) }
            );
        };
    });

});

//TODO: REFACTORING
const getResults = (room) => {
    let players = getPlayersInGame(room);

    if(players[0].card == '' || players[1].card == '') return false;

    if(players[0].card === "Paper") {
        if(players[1].card === "Rock") players[0].score++;
        if(players[1].card === "Scissors") players[1].score++;
    } else if(players[0].card === "Rock") {
        if(players[1].card === "Paper") players[1].score++;
        if(players[1].card === "Scissors") players[0].score++;
    } else if(players[0].card === "Scissors") {
        if(players[1].card === "Rock") players[1].score++;
        if(players[1].card === "Paper") players[0].score++;
    }

    updateCardOfPlayer(room, players[0].id, '');
    updateCardOfPlayer(room, players[1].id, '');

    updateScoreOfPlayer(room, players[0].id, players[0].score);
    updateScoreOfPlayer(room, players[1].id, players[1].score);

    return true;
}



/** Server initialitation  */
server.listen(PORT, () => {
    console.log(`Servidor escuchando al puerto ${PORT}`);
});