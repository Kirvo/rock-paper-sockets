const games = [];

class Game {
    /**
     * Game.id is User.room
     */
    id;

    /**
     * [, ]
    */
    players;
}

class Player {
    id;
    score;
    card;
}

const addGame = ({id, players = []}) => {
    id = id.trim().toLowerCase();

    const existingGame = games.find((game) => game.id === id);

    if(existingGame) {
        return { error: `Game is created` };
     }

    if(players.length === 0) {
        players = [
            { id: '', score: 0, card: '', username: '', icon: '' },
            { id: '', score: 0, card: '', username: '', icon: '' }
        ]
    }

    const game = { id, players };

    games.push(game);

    return { game };
}

const removeGame = (roomId) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        return games.splice(index, 1)[0];
    }
}

const updateScoreOfPlayer = (roomId, playerId, score) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        const indexPlayer = games[index].players.findIndex((p) => p.id === playerId);

        games[index].players[indexPlayer].score = score;
    }
}

const updateCardOfPlayer = (roomId, playerId, card) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        const indexPlayer = games[index].players.findIndex((p) => p.id === playerId);

        games[index].players[indexPlayer].card = card;
    }
}

const getCardOfPlayer = (roomId, playerId) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        const indexPlayer = games[index].players.findIndex((p) => p.id === playerId);
        
        return games[index].players[indexPlayer].card;
    }
}

const addPlayerToGame = (roomId, player) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        let aux = games[index].players;

        if(aux[0].id === '') games[index].players[0] = player;
        else if(aux[1].id === '') games[index].players[1] = player;
    }
}

const removePlayerInGame = (roomId, playerId) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        const indexPlayer = games[index].players.findIndex((p) => p.id === playerId);

        games[index].players[indexPlayer] = { id: '', score: 0, card: '', username: '', icon: '' };
    }
}

const getPlayersInGame = (roomId) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        return games[index].players;
    }
}

const getScoreInGame = (roomId) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        return [games[index].players[0].score, games[index].players[1].score];
    }
}

const getNamesInGame = (roomId) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        return [games[index].players[0].username, games[index].players[1].username];
    }
}

const getIconsInGame = (roomId) => {
    const index = games.findIndex((game) => game.id === roomId);

    if(index !== -1) {
        const iconP1 = games[index].players[0].icon;
        const iconP2 = games[index].players[1].icon;

        return [iconP1 === '' ? 'dragon' : iconP1, iconP1 === '' ? 'otter' : iconP2];
    }
}



module.exports = {
    addGame, removeGame, updateScoreOfPlayer, updateCardOfPlayer, getCardOfPlayer, addPlayerToGame,
    removePlayerInGame, getPlayersInGame, getScoreInGame, getNamesInGame, getIconsInGame
};